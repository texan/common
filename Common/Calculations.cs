﻿namespace Common
{
    public class Calculations
    {
        public static short Number { get; private set; }

        public static double AddHalf (double x)
        {
            double half = x / 2;
            x += half;
            return x;
        }

        public static double SubHalf(double x)
        {
            double half = x / 2;
            x -= half;
            return x;
        }

        public static int GCD (int a, int b)
        {
            if (b == 0)
                return a;

            int c = a % b;

            return GCD(b, c);
        }
    }
}
